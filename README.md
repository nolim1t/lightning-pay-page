# Lightning Pay Page

## About

This is a module for nolim1t.co, but I guess you can use it anywhere if you use the same template

## Usage

These templates are jekyll templates, although they may work with other static generators.

### Code

At the bottom someone put in the following below - reason being is that your page should fully load first, before we execute stuff

```
<script src="//nolim1t.co/js/addon-btc-lightning.js"></script>
```
